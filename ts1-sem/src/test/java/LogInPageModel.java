import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPageModel {
    private final WebDriver driver;

    public LogInPageModel(WebDriver driver) {
        this.driver = driver;
        this.driver.get("https://idos.idnes.cz/en/prihlaseni/?returnUrl=https%3A//idos.idnes.cz/vlakyautobusymhdvse/spojeni/");
    }

    public void setEmail(String emailStr) {
        WebElement email = driver.findElement(By.xpath("//*[@id=\"Email\"]"));
        email.sendKeys(emailStr);
    }

    public void setPassword(String passwordStr) {
        WebElement password = driver.findElement(By.xpath("//*[@id=\"Password\"]"));
        password.sendKeys(passwordStr);
    }

    public void logON() {
        WebElement logOn = driver.findElement(By.xpath("//*[@id=\"col-content\"]/div/form/div[1]/button"));
        logOn.click();
    }

    public String getAccountName() {
        return driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/p/a[1]")).getText();
    }

    public String getErrorMessage() {
        return driver.findElement(By.xpath("//*[@id=\"col-content\"]/div/form/p[1]/label")).getText();
    }
}
