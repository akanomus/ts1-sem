import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class idosczTest {
    WebDriver driver;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Test
    void getTitleTest() {
        driver.get("https://idos.idnes.cz/en/kosik/");

        String header = driver.getTitle();

        assertEquals(header, "IDOS • Cart");
    }

    @Test
    void timetableSelectionTest() {
        driver.get("https://idos.idnes.cz/en/vlakyautobusymhdvse/spojeni/");

        CookiesPageModel.skip(driver);

        WebElement selection = driver.findElement(By.xpath("//*[@id=\"timetablesModalLink\"]"));
        selection.click();

        String header = (new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(
                By.xpath("//*[@id=\"connTabContent\"]/div/div[3]/div/h3")))).getText();

        assertEquals(header, "Integrated Transport Systems");
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/get_element_name_test_data.csv"})
    void getElementNameTest(String element, String expectedName) {
        driver.get("https://idos.idnes.cz/en/vlakyautobusymhdvse/spojeni/");

        String name = driver.findElement(By.xpath(element)).getText();

        assertEquals(name, expectedName);
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/find_connections_test_data.csv"})
    void findConnectionsTest(String from, String to) {
        driver.get("https://idos.idnes.cz/en/vlakyautobusymhdvse/spojeni/");

        CookiesPageModel.skip(driver);

        WebElement sourceInput = driver.findElement(By.id("From"));
        sourceInput.sendKeys("Prague");

        WebElement destinationInput = driver.findElement(By.id("To"));
        destinationInput.sendKeys("Brno");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"connection-filter\"]/div[8]/button"));
        searchButton.click();

        WebElement connection = driver.findElement(By.cssSelector("div[class='connection-head']"));

        assertNotNull(ExpectedConditions.elementToBeClickable(connection));
    }

    @Test
    void logOnWithoutInputTest() {
        LogInPageModel logInPageModel = new LogInPageModel(this.driver);

        CookiesPageModel.skip(driver);

        logInPageModel.logON();

        assertEquals(logInPageModel.getErrorMessage(), "E-MAILEnter the e-mail");
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/log_on_with_input_test_data.csv"})
    void logOnWithInputTest(String provEmail, String provPassword, String excpectedName) {
        LogInPageModel logInPageModel = new LogInPageModel(this.driver);

        CookiesPageModel.skip(driver);

        logInPageModel.setEmail(provEmail);
        logInPageModel.setPassword(provPassword);

        logInPageModel.logON();

        assertEquals(logInPageModel.getAccountName(), excpectedName);
    }

    @Test
    void registrationWithoutInputTest() {
        RegistrationPageModel registrationPageModel = new RegistrationPageModel(this.driver);

        CookiesPageModel.skip(driver);

        registrationPageModel.clickCreateAccountButton();

        assertEquals(registrationPageModel.getInputErrorMessage(),
                "E-MAILEnter the e-mail");
    }

    @Test
    void registrationWithoutTermsTest() {
        RegistrationPageModel registrationPageModel = new RegistrationPageModel(this.driver);

        CookiesPageModel.skip(driver);

        registrationPageModel.clickCreateAccountButton();

        assertEquals(registrationPageModel.getTermsErrorMessage(),
                "Please confirm the Term and Conditions.");
    }

    @Test
    void toTheCartTest() {
        driver.get("https://idos.idnes.cz/en/vlakyautobusymhdvse/spojeni/vysledky/?f=Praha&fc=1&t=Brno&tc=1");

        CookiesPageModel.skip(driver);

        WebElement goToCartBtn = driver.findElement(By.cssSelector("a[href='javascript:;'][class='btn btn-buy btn-lightgreen btn-shadow ico-basket ']"));
        goToCartBtn.click();

        WebElement noRevDir = driver
                .findElement(By.xpath("//*[@id=\"isBackNo\"]"));
        noRevDir.click();

        WebElement header = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(
                By.xpath("//*[@id=\"bg-color\"]/div[1]/div[3]/h1")));

        assertEquals(header.getText(), "Cart");
    }

    @Test
    void discountTest() {
        driver.manage().window().maximize();
        String nextDay = DateModel.getNextDay();
        driver.get("https://idos.idnes.cz/en/vlakyautobusymhdvse/spojeni/vysledky/?date="
                + nextDay + "&time=12:06&f=Praha&fc=1&t=Brno&tc=1");

        CookiesPageModel.skip(driver);

        String priceStr = driver.findElement(By.cssSelector("span[class='price-value']")).getText();
        int priceWithoutDiscount = Integer.parseInt(priceStr);

        WebElement editPassenger = driver.findElement(By.xpath("//*[@id=\"ResultYourWay\"]/div/div/p/a"));
        editPassenger.click();

        WebElement smartcards = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(
                By.cssSelector("a[href='javascript:;'][class='passengers_reduction1']")));
        smartcards.click();

        WebElement in25 = driver.findElement(By.xpath("//*[@id=\"recud-options-1\"]/li[2]/label"));
        in25.click();

        WebElement saveRed = driver.findElement(By.id("save-reduction"));
        saveRed.click();


        WebElement savePassenger = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(
                By.id("save-passengers")));
        savePassenger.click();

        priceStr = (new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(
                By.cssSelector("span[class='price-value']")))).getText();
        int priceWithDiscount = Integer.parseInt(priceStr);

        assertEquals((priceWithDiscount * 100)/priceWithoutDiscount, 79);
    }


}
