import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegistrationPageModel {
    private final WebDriver driver;


    public RegistrationPageModel(WebDriver driver) {
        this.driver = driver;
        driver.get("https://idos.idnes.cz/en/registrace/");
    }

    public void clickCreateAccountButton() {
        WebElement createAcc = driver
                .findElement(By.xpath("//*[@id=\"bg-color\"]/div[1]/div[2]/div/div/div/form/p[8]/button"));
        createAcc.click();
    }

    public String getInputErrorMessage() {
        return driver
                .findElement(By.xpath("//*[@id=\"bg-color\"]/div[1]/div[2]/div/div/div/form/p[1]/label"))
                .getText();
    }

    public String getTermsErrorMessage() {
        return driver
                .findElement(By.xpath("//*[@id=\"agreeWithError\"]"))
                .getText();
    }
}
