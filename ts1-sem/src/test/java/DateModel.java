import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateModel {

    public static String getNextDay() {
        LocalDate currentDate = LocalDate.now();
        LocalDate nextDay = currentDate.plusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return nextDay.format(formatter);
    }
}
